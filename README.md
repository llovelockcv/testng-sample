# testng-sample
This project is to demonstrate how to create TestNG for Java agent with an TCM Automation Integration. It includes sample java code built with TestNG framework.

You can compile this source code with Maven or Ant.

### How to integrate this project with TCM Automation Host
1. Download this TestNG project and unzip in your directory (eg: D:\Demo\testng-sample).
2. Open command line on Windows or Termincal on Linux/Mac. Go to directory D:\Demo\testng-sample and execute command: mvn clean compile package test
